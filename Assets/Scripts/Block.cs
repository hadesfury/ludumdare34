﻿using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public enum CardinalType
    {
        NORTH,
        EAST,
        SOUTH,
        WEST    
    }

    public bool isLocked;

    [HideInInspector] public int xIndex;
    [HideInInspector] public int yIndex;

    private District parentDistrict;
    private readonly Dictionary<CardinalType, Block> neighbourTable = new Dictionary<CardinalType, Block>();

    [HideInInspector] public Building building;
    [HideInInspector] public Builder builder;
    [HideInInspector] public Converter converter;
    [HideInInspector] public Tanker tanker;

    public Dictionary<CardinalType, Block> GetNeighborTable()
    {
        if( neighbourTable.Count == 0 )
        {
            neighbourTable.Add( CardinalType.NORTH,  GameManager.gameManager.GetBlock( xIndex + 1, yIndex ) );
            neighbourTable.Add( CardinalType.EAST,  GameManager.gameManager.GetBlock( xIndex, yIndex + 1 ) );
            neighbourTable.Add( CardinalType.SOUTH,  GameManager.gameManager.GetBlock( xIndex - 1, yIndex ) );
            neighbourTable.Add( CardinalType.WEST,  GameManager.gameManager.GetBlock( xIndex, yIndex - 1 ) );
        }

        return neighbourTable;
    }

    void OnMouseEnter()
    {
        GameManager.gameManager.guiManager.statPanelLayout.DisplayDistrictStat( GetParentDistrict() );

        if( GetParentDistrict() != null )
        {
            int layer_index = LayerMask.NameToLayer( "Highlight" );

            GetParentDistrict().gameObject.layer = layer_index;

            foreach( Transform child_transform in GetParentDistrict().GetComponentsInChildren<Transform>() )
            {
                child_transform.gameObject.layer = layer_index;
            }
        }
    }

    void OnMouseExit()
    {
        GameManager.gameManager.guiManager.statPanelLayout.DisplayDistrictStat( null );
        
        if( GetParentDistrict() != null )
        {
            int layer_index = LayerMask.NameToLayer( "Default" );

            GetParentDistrict().gameObject.layer = layer_index;

            foreach( Transform child_transform in GetParentDistrict().GetComponentsInChildren<Transform>() )
            {
                child_transform.gameObject.layer = layer_index;
            }
        }
    }
    
    public void SetParentDistrict( District par_parent_district )
    {
        parentDistrict = par_parent_district;
    }

    public void Initialize( Building block_building )
    {
        block_building.transform.SetParent( transform, false );

        building = block_building;

        BuilderConfiguration builder_configuration = block_building.GetComponent<BuilderConfiguration>();
        if(builder_configuration != null )
        {
            builder = block_building.gameObject.AddComponent< Builder >();
            builder.configuration = builder_configuration;
        }

        ConverterConfiguration converter_configuration = block_building.GetComponent<ConverterConfiguration>();
        if(builder_configuration != null)
        {
            converter = block_building.gameObject.AddComponent<Converter>();
            converter.configuration = converter_configuration;
        }

        TankerConfiguration tanker_configuration = block_building.GetComponent<TankerConfiguration>();
        if(tanker_configuration != null)
        {
            tanker = block_building.gameObject.AddComponent<Tanker>();
            tanker.configuration = tanker_configuration;
        }
    }

    public District GetParentDistrict()
    {
        return parentDistrict;
    }
}
