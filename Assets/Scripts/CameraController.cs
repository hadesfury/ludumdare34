﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraTarget;
    [Header( "Pan" )]
    public float panSpeed;
    public float dragPanSpeed = 20.0f;
    public float maxPan = 75.0f;
    [Header("Zoom")]
    public float minZoom = 1;
    public float maxZoom = 10;
    public float zoomStep = 1;
    public float ctrlZoomSpeed = 10.0f;

    private new Camera camera;
    private Vector3 previousMousePosition;

    void Awake()
    {
        camera = GetComponent<Camera>();
    }

    void Update()
    {
        {
            float horizontal_axis = Input.GetAxis( "Horizontal" );
            float vertical_axis = Input.GetAxis( "Vertical" );
            Vector3 movement_vector = new Vector3( horizontal_axis, vertical_axis, 0 );

            movement_vector.Normalize();

            cameraTarget.Translate( movement_vector * panSpeed * Time.deltaTime * camera.orthographicSize );
        }

        {
            if( Input.GetMouseButtonDown( 0 ) )
            {
                previousMousePosition = Input.mousePosition;
            }

            if( Input.GetMouseButton( 0 ) )
            {
                Vector3 delta_mouse = Input.mousePosition - previousMousePosition;

                if( Input.GetKey( KeyCode.LeftControl )
                    || Input.GetKey( KeyCode.RightControl ) )
                {
                    float current_zoom_level = camera.orthographicSize;

                    if( delta_mouse.y < 0 )
                    {
                        current_zoom_level += zoomStep * Time.deltaTime * ctrlZoomSpeed;
                    }
                    else if( delta_mouse.y > 0 )
                    {
                        current_zoom_level -= zoomStep * Time.deltaTime * ctrlZoomSpeed;
                    }

                    camera.orthographicSize = Mathf.Clamp( current_zoom_level, minZoom, maxZoom );
                }
                else
                {
                    cameraTarget.Translate( delta_mouse * -panSpeed * Time.deltaTime * camera.orthographicSize ); // = camera.ScreenToWorldPoint( Input.mousePosition );//   
                }

                previousMousePosition = Input.mousePosition;
            }

            if( Input.GetMouseButtonUp( 0 ) )
            {
                previousMousePosition = Vector3.zero;
            }
        }

        #if!UNITY_EDITOR
        {
            float horizontal_axis = 0;
            float vertical_axis = 0;
            Vector3 viewport_mouse_position = camera.ScreenToViewportPoint( Input.mousePosition );

            if( viewport_mouse_position.x <= 0 )
            {
                horizontal_axis = -1;
            }
            else if( viewport_mouse_position.x >= 1 )
            {
                horizontal_axis = 1;
            }

            if( viewport_mouse_position.y <= 0 )
            {
                vertical_axis = -1;
            }
            else if( viewport_mouse_position.y >= 1 )
            {
                vertical_axis = 1;
            }

            Vector3 movement_vector = new Vector3( horizontal_axis, vertical_axis, 0 );

            movement_vector.Normalize();

            cameraTarget.Translate( movement_vector * panSpeed * Time.deltaTime );
        }
        #endif
        //cameraTarget.transform.localPosition.Set( Mathf.Clamp( cameraTarget.transform.position.x, -maxPan, +maxPan ), Mathf.Clamp( cameraTarget.transform.position.y, -maxPan, +maxPan ), 0 );

        {
            float mouse_scroll_wheel = Input.GetAxis( "Mouse ScrollWheel" );
            float current_zoom_level = camera.orthographicSize;

            if( mouse_scroll_wheel < 0 )
            {
                current_zoom_level += zoomStep;
            }
            else if( mouse_scroll_wheel > 0 )
            {
                current_zoom_level -= zoomStep;
            }

            camera.orthographicSize = Mathf.Clamp( current_zoom_level, minZoom, maxZoom );
        }
    }
}
