﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    public RectTransform startMenu;
    public LeaderboardLayout leaderboardLayout;
    public QuestionPanelLayout questionPanelLayout;
    public StatPanelLayout statPanelLayout;
    public RectTransform gameOver;
    public RectTransform scorePanel;
    public Text scoreText;
    public InputField nameInputField;

    void Awake()
    {
        questionPanelLayout.gameObject.SetActive( false );
        statPanelLayout.gameObject.SetActive( false );
        gameOver.gameObject.SetActive( false );
        scorePanel.gameObject.SetActive( false );
        leaderboardLayout.gameObject.SetActive( false );
    }

    //void Start()
    //{
    //    //Leaderboard leaderboard = new Leaderboard();
    //    //LeaderboardScoreEntry leaderboard_score_entry = new LeaderboardScoreEntry();

    //    //leaderboard_score_entry.playerName = "test";
    //    //leaderboard_score_entry.date = "test";

    //    //leaderboard.table.Add( leaderboard_score_entry );
    //    //leaderboard_score_entry = new LeaderboardScoreEntry();
    //    //leaderboard_score_entry.playerName = "test2";
    //    //leaderboard_score_entry.date = "test2";

    //    //leaderboard.table.Add( leaderboard_score_entry );
    //    StartCoroutine( GetLeaderBoard() );

    //    //print( JsonUtility.ToJson( leaderboard ) );
    //}

    public void DisplayScore( Dictionary<CapabilityType, int> par_capabilities_score_table )
    {
        int ecology_score = par_capabilities_score_table[CapabilityType.EDUCATION] + par_capabilities_score_table[CapabilityType.HEALTH] - par_capabilities_score_table[CapabilityType.GROWTH];
        int wealth_score = par_capabilities_score_table[ CapabilityType.EDUCATION ] + par_capabilities_score_table[ CapabilityType.MONEY ];

        int max_block_count = GameManager.gameManager.maxDistrictValue * GameManager.gameManager.resourceManager.districtPrefab.maximumNumberOfBlocks;
        int current_block_count = 0;

        foreach( District district in GameManager.gameManager.districtTable )
        {
            current_block_count += district.blockTable.Count;
        }

        scoreText.text = string.Format( "Ecology : {0} / Wealth : {1}\n {2} block(s) remaining", ecology_score * 100, wealth_score * 100, max_block_count - current_block_count );
    }
    
    public void DisplayLeaderBoard()
    {
        leaderboardLayout.gameObject.SetActive( !leaderboardLayout.gameObject.activeSelf );
    }
}
