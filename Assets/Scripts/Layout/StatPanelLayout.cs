﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatPanelLayout : MonoBehaviour
{
    public enum DisplayMode
    {
        BUILDING_COUNT,
        INFLUENCING,
        RESOURCES
    }

    public Text text;

    private DisplayMode displayMode = DisplayMode.BUILDING_COUNT;
    private Dictionary<Building.BuildingType, Dictionary<int, int>> buildingCountTable = new Dictionary<Building.BuildingType, Dictionary<int, int>>();

    private District currentDistrict;

    void Awake()
    {
        foreach( Building.BuildingType building_type in Enum.GetValues( typeof( Building.BuildingType ) ) )
        {
            if( !buildingCountTable.ContainsKey( building_type ) )
            {
                buildingCountTable.Add( building_type, new Dictionary<int, int>() );
            }
        }
    }

    public void DisplayDistrictStat( District par_parent_district )
    {
        if( par_parent_district != null )
        {
            currentDistrict = par_parent_district;

            switch( displayMode )
            {
                case DisplayMode.BUILDING_COUNT:
                {
                    DisplayBuidingCount( par_parent_district );
                    break;
                }
                case DisplayMode.INFLUENCING:
                {
                    DisplayInfluencing( par_parent_district );
                    break;
                }
                default:
                {
                    DisplayResources( par_parent_district );
                    break;
                }
            }
        }
    }

    private void CleanBuildingCountTable()
    {
        foreach( Building.BuildingType building_type in Enum.GetValues( typeof( Building.BuildingType ) ) )
        {
            buildingCountTable[ building_type ].Clear();
        }
    }

    public void SetDiplayModeBuildingCount()
    {
        SetDisplayMode( DisplayMode.BUILDING_COUNT );
    }

    public void SetDiplayModeInfluencing()
    {
        SetDisplayMode( DisplayMode.INFLUENCING );
    }

    public void SetDiplayModeResources()    
    {
        SetDisplayMode( DisplayMode.RESOURCES );
    }

    private void SetDisplayMode( DisplayMode par_display_mode )
    {
        displayMode = par_display_mode;
        DisplayDistrictStat( currentDistrict );
    }

    private void DisplayBuidingCount( District par_parent_district )
    {
        StringBuilder string_builder = new StringBuilder();
        
        string_builder.AppendFormat( "District {0}\n", par_parent_district.districtId );
        CleanBuildingCountTable();
        
        foreach( Block block in par_parent_district.blockTable )
        {
            Building.BuildingType current_building_type = block.building.buildingType;
            int current_building_level = block.building.buildingLevel;
            Dictionary<int, int> building_type_table = buildingCountTable[ current_building_type ];

            if( !building_type_table.ContainsKey( current_building_level ) )
            {
                building_type_table.Add( current_building_level, 1 );
            }
            else
            {
                building_type_table[ current_building_level ] += 1;
            }
        }

        foreach( Building.BuildingType building_type in Enum.GetValues( typeof( Building.BuildingType ) ) )
        {
            int max_level = District.GetMaxBuildingLevel( building_type );
            Dictionary<int, int> building_level_count_table = buildingCountTable[ building_type ];

            string_builder.AppendFormat( "Type {0}\n", building_type );


            for( int level_index = 0; level_index < max_level + 1; level_index++ )
            {
                int current_level_value = 0;
                if( building_level_count_table.ContainsKey( level_index ) )
                {
                    current_level_value = building_level_count_table[ level_index ];
                }
                string_builder.AppendFormat( "{1}, ", level_index, current_level_value );
            }
            string_builder.AppendFormat( "\n");
        }

        text.text = string_builder.ToString();
    }

    private void DisplayInfluencing( District par_parent_district )
    {
        StringBuilder string_builder = new StringBuilder();
        string_builder.AppendFormat( "District {0}\n", par_parent_district.districtId );

        foreach( KeyValuePair<District.PolicyType, int> district_policy in par_parent_district.districtPolicyTable )
        {
            string_builder.AppendFormat( "{0} {1}\n", district_policy.Key, district_policy.Value );
        }

        text.text = string_builder.ToString();
    }

    private void DisplayResources( District par_parent_district )
    {
        StringBuilder string_builder = new StringBuilder();

        string_builder.AppendFormat( "District {0}\n", par_parent_district.districtId );

        foreach( KeyValuePair<CapabilityType, int> district_policy in par_parent_district.capabilitiesScoreTable )
        {
            string_builder.AppendFormat( "{0} {1}\n", district_policy.Key, district_policy.Value );
        }

        text.text = string_builder.ToString();

    }

}
