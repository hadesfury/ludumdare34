﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LeaderboardLayout : MonoBehaviour
{
    public Text ecologyLeaderboardText;
    public Text wealthLeaderboardText;

    public int maxLeaderboardsEntryCount = 20;

    void OnEnable()
    {
        ecologyLeaderboardText.text = "Loading ...";
        wealthLeaderboardText.text = "Loading ...";
        StartCoroutine( GetLeaderBoard() );
    }

    [Serializable]
    private class Leaderboard
    {
        public List<LeaderboardScoreEntry> table = new List<LeaderboardScoreEntry>();
    }
    [Serializable]
    private class LeaderboardScoreEntry
    {
        public string date;
        public string gameRef;
        public string playerName;
        public string score;
        public string _id;
        public string __v;
    }
    private IEnumerator GetLeaderBoard()
    {
        //string
        //   json = string.Format( "{{\"playerName\": \"{0}\",\"gameRef\": \"{1}\" ,\"score\": \"{2}\"}}", playerName, "URBO_" + score_name, score );
        //Dictionary<string, string>
        //    header_table = new Dictionary<string, string>();

        //header_table.Add( "Content-Type", "application/json" );

        //byte[]
        //    json_data = Encoding.ASCII.GetBytes( json.ToCharArray() );
        WWW
            www = new WWW( "http://ld34-ludumdare32.rhcloud.com/score" );

        yield return www;

        print( www.text );

        Leaderboard leaderboard_table = JsonUtility.FromJson<Leaderboard>( "{\"table\":" + www.text + "}" );

        StringBuilder ecology_leaderboard_builder = new StringBuilder();
        StringBuilder wealth_leaderboard_builder = new StringBuilder();
        List<LeaderboardScoreEntry> ecology_leaderboard = new List<LeaderboardScoreEntry>();
        List<LeaderboardScoreEntry> wealth_leaderboard = new List<LeaderboardScoreEntry>();

        foreach( LeaderboardScoreEntry leaderboard_score_entry in leaderboard_table.table )
        {
            if( leaderboard_score_entry.gameRef == "URBO_ecology" )
            {
                ecology_leaderboard.Add( leaderboard_score_entry );
            }
            else if( leaderboard_score_entry.gameRef == "URBO_wealth" )
            {
                wealth_leaderboard.Add( leaderboard_score_entry );
            }
        }

        ecology_leaderboard.Sort( SortLeaderboard );
        wealth_leaderboard.Sort( SortLeaderboard );

        int leaderboard_max_entry = maxLeaderboardsEntryCount;

        foreach( LeaderboardScoreEntry leaderboard_score_entry in ecology_leaderboard )
        {
            ecology_leaderboard_builder.AppendFormat( "{0} - {1}\n", leaderboard_score_entry.playerName, leaderboard_score_entry.score, leaderboard_score_entry.date );
            --leaderboard_max_entry;
            if( leaderboard_max_entry <= 0 )
            {
                break;
            }
        }

        leaderboard_max_entry = maxLeaderboardsEntryCount;

        foreach( LeaderboardScoreEntry leaderboard_score_entry in wealth_leaderboard )
        {
            wealth_leaderboard_builder.AppendFormat( "{0} - {1}\n", leaderboard_score_entry.playerName, leaderboard_score_entry.score, leaderboard_score_entry.date );
            --leaderboard_max_entry;
            if( leaderboard_max_entry <= 0 )
            {
                break;
            }
        }

        ecologyLeaderboardText.text = ecology_leaderboard_builder.ToString();
        wealthLeaderboardText.text = wealth_leaderboard_builder.ToString();
    }

    private int SortLeaderboard( LeaderboardScoreEntry par_x, LeaderboardScoreEntry par_y )
    {
        return int.Parse( par_y.score ) - int.Parse( par_x.score );
    }
}
