﻿using UnityEngine;
using UnityEngine.UI;

public class QuestionPanelLayout : MonoBehaviour
{
    public Button button1;
    public Button button2;
    public Text questionText;
    public Text buttonText1;
    public Text buttonText2;

    [HideInInspector] public float lastPopUpTime;
    private District currentDistrict;
    private District.PolicyType buttonResult1;
    private District.PolicyType buttonResult2;
    [HideInInspector] public bool doesLockQuestion = false;


    public void Reset()
    {
        GameManager.gameManager.gameObject.SetActive( true );
        gameObject.SetActive( false );
        currentDistrict = null;
        //buttonResult1 = null;
        //buttonResult2 = null;

        doesLockQuestion = false;

        lastPopUpTime = Time.realtimeSinceStartup;
    }

    public void OnButton1()
    {
        currentDistrict.IncreasePolicy( buttonResult1 );
        Reset();
    }
    public void OnButton2()
    {
        currentDistrict.IncreasePolicy( buttonResult2 );
        Reset();
    }

    public void SetQuestion( string question, District district, District.PolicyType first_choice, District.PolicyType second_choice )
    {
        //Reset();
        GameManager.gameManager.gameObject.SetActive( false );

        currentDistrict = district;

        questionText.text = question;

        buttonText1.text = first_choice.ToString();
        buttonText2.text = second_choice.ToString();

        buttonResult1 = first_choice;
        buttonResult2 = second_choice;
    }

    void Update()
    {
        GameManager.gameManager.lastDecisionTime = Time.realtimeSinceStartup;
    }
}
