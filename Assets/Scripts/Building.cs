﻿using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour
{
    public enum BuildingType
    {
        RESIDENTIAL,
        COMMERCIAL,
        INDUSTRY,
        EDUCATION,
        HEALTH,
        SECURITY
    }
    
    public BuildingType buildingType;
    public int buildingLevel;

    [HideInInspector] public int probability = 0;
}
