﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class District : MonoBehaviour
{
    public enum PolicyType
    {
        EDUCATION,
        GROWTH,
        HEALTH,
        MONEY
    }

    public Canvas uiButton;

    public int maximumNumberOfBlocks = Int32.MaxValue;
    [HideInInspector] public int numberOfBlocks = 0;
    [HideInInspector] public int numberOfTicks = 0;

    [HideInInspector] public int districtId;
    [HideInInspector] public Color districtColor;
    [HideInInspector] public List<Block> blockTable = new List<Block>();
    [HideInInspector] public bool isFull = false;

    public readonly Dictionary<CapabilityType,int> capabilitiesScoreTable = new Dictionary<CapabilityType, int>();

    public readonly Dictionary<Building.BuildingType, int> oldDistrictPolicyTable = new Dictionary<Building.BuildingType, int>();
    public readonly Dictionary<PolicyType, int> districtPolicyTable = new Dictionary<PolicyType, int>();
    public readonly Dictionary<PolicyType, List<Building.BuildingType>> policyBuildingTable = new Dictionary<PolicyType, List<Building.BuildingType>>(); 

    private Dictionary<ResourceType, int> sharedResources = new Dictionary<ResourceType, int>();
    private readonly Dictionary<CapabilityType, int> sharedCapabilities = new Dictionary<CapabilityType, int>();
    private readonly List<GameObject> borderTable = new List<GameObject>();

    private PolicyType firstPolicyChoice;
    private PolicyType secondPolicyChoice;

    void Awake()
    {
        oldDistrictPolicyTable.Add( Building.BuildingType.RESIDENTIAL, 1 );
        uiButton.gameObject.SetActive( false );

        foreach( PolicyType policy_type in Enum.GetValues( typeof( PolicyType ) ) )
        {
            districtPolicyTable.Add( policy_type, 0 );
            policyBuildingTable.Add( policy_type, new List<Building.BuildingType>() );
        }

        policyBuildingTable[ PolicyType.EDUCATION ].Add( Building.BuildingType.INDUSTRY );
        policyBuildingTable[ PolicyType.EDUCATION ].Add( Building.BuildingType.EDUCATION );

        policyBuildingTable[ PolicyType.GROWTH ].Add( Building.BuildingType.INDUSTRY );
        policyBuildingTable[ PolicyType.GROWTH ].Add( Building.BuildingType.COMMERCIAL );
        policyBuildingTable[ PolicyType.GROWTH ].Add( Building.BuildingType.RESIDENTIAL );

        policyBuildingTable[ PolicyType.HEALTH ].Add( Building.BuildingType.COMMERCIAL );
        policyBuildingTable[ PolicyType.HEALTH ].Add( Building.BuildingType.EDUCATION );
        policyBuildingTable[ PolicyType.HEALTH ].Add( Building.BuildingType.HEALTH );
        policyBuildingTable[ PolicyType.HEALTH ].Add( Building.BuildingType.SECURITY );

        policyBuildingTable[ PolicyType.MONEY ].Add( Building.BuildingType.INDUSTRY );
        policyBuildingTable[ PolicyType.MONEY ].Add( Building.BuildingType.RESIDENTIAL );
        policyBuildingTable[ PolicyType.MONEY ].Add( Building.BuildingType.HEALTH );
    }

    public void Tick( bool does_create_new_block )
    {
        // Game Loop
        //

        // 1 - Execute Workers TODO not implemented yet !
        /*
        foreach (Block block in blockTable)
        {
            if (block.worker != null)
            {
                block.worker.Work(1, ref sharedResources);
            }
        }
        */

        // 2 - Execute Builders
        ExecuteBuilders();

        // 3 - Execute Converters
        Dictionary<ResourceType, int> shared_produced_resources = ExecuteConverters();

        // 4 - Execute Tanker - Dispatch produced resources in available tankers
        ExtractTanker( shared_produced_resources );

        // INFO - Refresh GameObject label for debug purpose
        RefreshStats();

        UpdateScore();

        if( does_create_new_block )
        {
            // ? - Suggest a new Action
            // TODO implement random logic to suggest 
            if( numberOfBlocks < maximumNumberOfBlocks )
            {
                ChooseNextBlock();
            }
            else
            {
                isFull = true;
            }
        }
        numberOfTicks++;
    }

    private void UpdateScore()
    {
        capabilitiesScoreTable.Clear();
        
        foreach( CapabilityType capability in Enum.GetValues( typeof( CapabilityType ) ) )
        {
            capabilitiesScoreTable.Add( capability, 0 );
        }

        foreach( Block block in blockTable )
        {
            List<CapabilityTypeQuantityDictionaryEntry> produced_capability = block.builder.configuration._producedCapabilities;

            foreach( CapabilityTypeQuantityDictionaryEntry capability_type_quantity in produced_capability )
            {
                capabilitiesScoreTable[ capability_type_quantity.key ] += capability_type_quantity.value;
            }
        }
    }

    private void ExecuteBuilders()
    {
        Dictionary<ResourceType, int> shared_consumed_resources = new Dictionary<ResourceType, int>();

        sharedCapabilities.Clear();

        foreach( Block block in blockTable )
        {
            if( block.builder != null )
            {
                // Update district capabilities
                if( block.builder.isCompleted )
                {
                    foreach( CapabilityTypeQuantityDictionaryEntry capability_type_quantity in block.builder.configuration._producedCapabilities )
                    {
                        if( !sharedCapabilities.ContainsKey( capability_type_quantity.key ) )
                        {
                            sharedCapabilities.Add( capability_type_quantity.key, 0 );
                        }

                        sharedCapabilities[ capability_type_quantity.key ] += capability_type_quantity.value;
                    }
                }
                else
                {
                    Dictionary<ResourceType, int> consumed_resources = block.builder.Build( 1, ref sharedResources );
                    shared_consumed_resources = ResourceUtils.MergeResources( shared_consumed_resources, consumed_resources );
                }
            }
        }

        // Dispatch consumed resources in available Tanker
        // TODO create a share resource Pool instead of doing it twice !
        foreach( KeyValuePair<ResourceType, int> shared_consumed_resource in shared_consumed_resources )
        {
            int to_consume_quantity = shared_consumed_resource.Value;
            int total_consumed_quantity = 0;
            foreach( Block block in blockTable )
            {
                if( block.tanker != null )
                {
                    int consumed_quantity = block.tanker.Pop( shared_consumed_resource.Key, to_consume_quantity );
                    to_consume_quantity -= consumed_quantity;
                    total_consumed_quantity += consumed_quantity;
                }
            }
        }
    }

    private Dictionary<ResourceType, int> ExecuteConverters()
    {
        Dictionary<ResourceType, int> shared_produced_resources = new Dictionary<ResourceType, int>();
        foreach( Block block in blockTable )
        {
            if( block.converter != null )
            {
                if( ( block.builder == null )
                    || block.builder.isCompleted )
                {
                    Dictionary<ResourceType, int> produced_resources = block.converter.Convert( 1, ref sharedResources );
                    shared_produced_resources = ResourceUtils.MergeResources( shared_produced_resources, produced_resources );
                }
            }
        }
        // TODO Dispatch consumed resources in available Tanker 
        return shared_produced_resources;
    }

    private void ExtractTanker( Dictionary<ResourceType, int> shared_produced_resources )
    {
        Dictionary<ResourceType, int> shared_stored_resources = new Dictionary<ResourceType, int>();
        Dictionary<ResourceType, int> wasted_stored_resources = new Dictionary<ResourceType, int>(); // TODO can be useful for stats !

        foreach( KeyValuePair<ResourceType, int> shared_produced_resource in shared_produced_resources )
        {
            ResourceType resource_to_store = shared_produced_resource.Key;
            int produced_quantity = shared_produced_resource.Value;
            int to_store_quantity = produced_quantity;
            int total_stored_quantity = 0;

            foreach( Block block in blockTable )
            {
                if( block.tanker != null )
                {
                    //Debug.Log( "Distrtic " + districtId  + " block " + block.builder.name);
                    int stored_quantity = block.tanker.Push( shared_produced_resource.Key, to_store_quantity );
                    total_stored_quantity += stored_quantity;
                    to_store_quantity -= stored_quantity;
                }
            }
            shared_stored_resources.Add( resource_to_store, total_stored_quantity );
            wasted_stored_resources.Add( resource_to_store, to_store_quantity );
            //Debug.Log( string.Format( "Resource {0} - produced= {1} | stored= {2} | wasted= {3}", resource_to_store, produced_quantity, total_stored_quantity, to_store_quantity ) );
        }
        sharedResources = ResourceUtils.MergeResources( sharedResources, shared_stored_resources );
    }

    private void ChooseNextBlock()
    {
        //QuestionPanelLayout question_panel = GameManager.gameManager.guiManager.questionPanelLayout;
        List<Building> accessible_building_table = ListAccessibleBlocks();

        if( accessible_building_table.Count > 0 )
        {
            Building next_building = GetNextBuilding();

            if( next_building != null )
            {
                BuildNextBlock( next_building );
            }
        }
    }

    private PolicyType GetDominantPolicyType()
    {
        List<PolicyType> dominant_policy_type_table = new List<PolicyType>();
        int dominant_policy_type_value = -1;

        foreach( KeyValuePair<PolicyType, int> current_policy_type in districtPolicyTable )
        {
            if( current_policy_type.Value > dominant_policy_type_value )
            {
                dominant_policy_type_value = current_policy_type.Value;
                dominant_policy_type_table.Clear();
                dominant_policy_type_table.Add( current_policy_type.Key );
            }
            else if( current_policy_type.Value == dominant_policy_type_value)
            {
                dominant_policy_type_table.Add( current_policy_type.Key );
            }
        }

        return dominant_policy_type_table[ Random.Range( 0, dominant_policy_type_table.Count ) ];
    }

    public static int GetMaxBuildingLevel(Building.BuildingType par_building_type)
    {
        int max_level = 0;
        List<Building> building_table = GameManager.gameManager.resourceManager.sortedBuildingTable[ par_building_type ];

        foreach( Building building in building_table )
        {
            max_level = Mathf.Max( max_level, building.buildingLevel );
        }

        return max_level;
    }

    private Building GetNextBuilding()
    {
        Building next_building = null;
        PolicyType dominant_policy_type = GetDominantPolicyType();
        Building.BuildingType building_type = policyBuildingTable[ dominant_policy_type ][ Random.Range( 0, policyBuildingTable[ dominant_policy_type ].Count ) ];
        int max_level = GetMaxBuildingLevel( building_type );
        Dictionary<int, int> building_count_table = ComputeBuildingCount( building_type );
        int selected_level = GetNextBuildingLevel( building_count_table, max_level );
        List<Building> current_level_building_table = new List<Building>();

        foreach( Building building in GameManager.gameManager.resourceManager.sortedBuildingTable[ building_type ] )
        {
            if( building.buildingLevel == selected_level )
            {
                current_level_building_table.Add( building );
            }
        }

        next_building = current_level_building_table[ Random.Range( 0, current_level_building_table.Count ) ];

        return next_building;
    }

    private int GetNextBuildingLevel( Dictionary<int, int> par_building_count_table, int max_level )
    {
        int[] required_level_table;
        int selected_level = 0;

        if( max_level >= 2 )
        {
            required_level_table = new[] { 1, 2, 3, 4, 5 };
        }
        else
        {
            required_level_table = new[] { 1, 3, 5 };
        }

        for( int current_level = 0; current_level < max_level; current_level++ )
        {
            int current_level_building_count = 0;
            int next_level_building_count = 0;

            if( par_building_count_table.ContainsKey( max_level - current_level ) )
            {
                current_level_building_count = par_building_count_table[ max_level - current_level ];
            }

            if( par_building_count_table.ContainsKey( (max_level - current_level) - 1 ) )
            {
                next_level_building_count = par_building_count_table[ (max_level - current_level) - 1 ];
            }

            if( ( 1 + current_level_building_count ) * required_level_table[ current_level + 1 ] < next_level_building_count )
            {
                break;
            }
            ++selected_level;
        }
        
        return max_level - selected_level;
    }

    private int GetRandomBuildingLevel( int max_level )
    {
        int[] probability_table;
        int probability_range = 0;
        int selected_level = 0;

        if( max_level >= 2 )
        {
            probability_table = new [] { 1, 2, 3, 4, 5 };
        }
        else
        {
            probability_table = new[] { 1, 3, 5 };
        }

        for( int current_level = 0; current_level < max_level + 1; current_level++ )
        {
            probability_range += probability_table[ current_level ];
        }

        int probability_value = Random.Range( 0, probability_range );

        for( int current_level = 0; current_level < max_level + 1; current_level++ )
        {
            probability_value -= probability_table[ current_level ];

            if( probability_value <= 0 )
            {
                selected_level = current_level;
                break;
            }
        }

        return max_level - selected_level;
    }

    private Dictionary<int, int> ComputeBuildingCount( Building.BuildingType building_type )
    {
        // Compute how many block of this type we have ... by level
        Dictionary<int, int> building_count_table = new Dictionary<int, int>();
        foreach( Block block in blockTable )
        {
            if( block.building.buildingType == building_type )
            {
                if( !building_count_table.ContainsKey( block.building.buildingLevel ) )
                {
                    building_count_table.Add( block.building.buildingLevel, 1 );
                }
                else
                {
                    building_count_table[ block.building.buildingLevel ] += 1;
                }
            }
        }

        return building_count_table;
    }

    //private int InitialProbabilityComputation( List<Building> accessible_building_table )
    //{
    //    int probability_range = 0;
    //    // compute block probability
    //    {
    //        int building_max_level = 5;

    //        foreach( Building building in accessible_building_table )
    //        {
    //            building.probability = 0;
    //            building.probability += building_max_level - building.buildingLevel;
    //            foreach( KeyValuePair<Building.BuildingType, int> district_policy in oldDistrictPolicyTable )
    //            {
    //                if( district_policy.Key == building.buildingType )
    //                {
    //                    building.probability += district_policy.Value;
    //                }
    //            }

    //            // block level ratio
    //            Dictionary<int, int> building_level_table = new Dictionary<int, int>();
    //            const int RATIO_FACTOR = 10;

    //            foreach( Block block in blockTable )
    //            {
    //                if( block.building.buildingType == building.buildingType )
    //                {
    //                    if( !building_level_table.ContainsKey( block.building.buildingLevel ) )
    //                    {
    //                        building_level_table.Add( block.building.buildingLevel, 1 );
    //                    }
    //                    else
    //                    {
    //                        building_level_table[ block.building.buildingLevel ] += 1;
    //                    }
    //                }
    //            }

    //            if( !building_level_table.ContainsKey( building.buildingLevel ) )
    //            {
    //                if( building.buildingLevel > 0 )
    //                {
    //                    if( building_level_table.ContainsKey( building.buildingLevel - 1 ) )
    //                    {
    //                        building.probability += building_level_table[ building.buildingLevel - 1 ] * RATIO_FACTOR;
    //                    }
    //                    else
    //                    {
    //                        building.probability = 0;
    //                    }
    //                }
    //                else
    //                {
    //                    //building.probability -= 2 * ration_factor;
    //                    //TODO perhaps reduce drasticly the proba
    //                }
    //            }
    //            else
    //            {
    //                if( building.buildingLevel > 0 )
    //                {
    //                    if( building_level_table.ContainsKey( building.buildingLevel - 1 )
    //                        && ( ( buildingLevelRatio * building_level_table[ building.buildingLevel ] ) < building_level_table[ building.buildingLevel - 1 ] )
    //                        )
    //                    {
    //                        building.probability += ( building_level_table[ building.buildingLevel - 1 ] - buildingLevelRatio * building_level_table[ building.buildingLevel ] )
    //                                                * RATIO_FACTOR;
    //                    }
    //                    else
    //                    {
    //                        building.probability = 0;
    //                    }
    //                }
    //                else
    //                {
    //                    if( building_level_table.ContainsKey( building.buildingLevel + 1 )
    //                        && building_level_table[ building.buildingLevel ] < buildingLevelRatio * building_level_table[ building.buildingLevel + 1 ]
    //                        )
    //                    {
    //                        building.probability += ( buildingLevelRatio * building_level_table[ building.buildingLevel + 1 ] - building_level_table[ building.buildingLevel ] )
    //                                                * RATIO_FACTOR;
    //                    }
    //                    else
    //                    {
    //                        //building.probability -= 2 * ration_factor;
    //                        //TODO perhaps reduce drasticly the proba
    //                    }
    //                }
    //            }
    //        }

    //        foreach( Building building in accessible_building_table )
    //        {
    //            //building.probability *= 4;
    //            probability_range += building.probability;
    //        }
    //    }
    //    return probability_range;
    //}

    //private Building GetRandomBuilding( List<Building> par_accessible_building_table, int par_probability_range )
    //{
    //    Building choosen_building = null;
    //    int probability_value = Random.Range( 0, par_probability_range );

    //    foreach( Building building in par_accessible_building_table )
    //    {
    //        probability_value -= building.probability;
            
    //        if( probability_value < 0 )
    //        {
    //            choosen_building = building;
    //            break;
    //        }
    //    }

    //    return choosen_building;
    //}

    public void BuildNextBlock( Building picked_prefab )
    {
        Block free_block = GetConstructibleBlock( picked_prefab );

        if( free_block != null )
        {
            if( free_block.building == null )
            {
                AddBlock( free_block, picked_prefab );
            }
            else
            {
                //Upgrade block
            }
        }
        GameManager.gameManager.guiManager.statPanelLayout.DisplayDistrictStat( this );
        //GameManager.gameManager.guiManager.questionPanelLayout.Reset();
    }

    public List<Building> ListAccessibleBlocks()
    {
        List<Building> accessibles_blocks = new List<Building>();

        foreach( Building potential_prefab in GameManager.gameManager.resourceManager.buildingTable )
        {
            BuilderConfiguration builder_configuration = potential_prefab.GetComponent<BuilderConfiguration>();

            if( builder_configuration != null )
            {
                if( builder_configuration.CanBuild( sharedResources, sharedCapabilities ) 
                    && oldDistrictPolicyTable.ContainsKey( potential_prefab.buildingType ) 
                    && oldDistrictPolicyTable[potential_prefab.buildingType] > potential_prefab.buildingLevel 
                    )
                {
                    accessibles_blocks.Add( potential_prefab );
                }
            }
        }

        return accessibles_blocks;
    }

    public void RefreshStats()
    {
        StringBuilder name_builder = new StringBuilder();
        name_builder.AppendFormat( "District {0} ", districtId );
        foreach( KeyValuePair<ResourceType, int> shared_resource in sharedResources )
        {
            name_builder.AppendFormat( " | ({0},{1})", shared_resource.Key, shared_resource.Value );
        }
        foreach( KeyValuePair<CapabilityType, int> shared_capability in sharedCapabilities )
        {
            name_builder.AppendFormat( " | ({0},{1})", shared_capability.Key, shared_capability.Value );
        }
        gameObject.name = name_builder.ToString();
    }

    public Block GetConstructibleBlock( Building selected_prefab )
    {
        Block constructible_block = null;
        List<Block> free_block_table = new List<Block>();

        //Increase district surface
        foreach( Block block in blockTable )
        {
            foreach( Block neighbor_block in block.GetNeighborTable().Values )
            {
                if( neighbor_block.GetParentDistrict() == null )
                {
                    free_block_table.Add( neighbor_block );
                }
            }
        }

        if( free_block_table.Count > 0 )
        {
            constructible_block = free_block_table[ Random.Range( 0, free_block_table.Count ) ];
        }
        
        return constructible_block;
    }

    public void Initialize( Block free_block )
    {
        List<Building> accessible_prefabs = ListAccessibleBlocks();

        if( accessible_prefabs.Count > 0 )
        {
            Building selected_prefab = accessible_prefabs[ 0 ];
            // Default resources
            sharedResources.Add( ResourceType.MONEY, 500 );

            // Add default block
            AddBlock( free_block, selected_prefab );
        }

        RefreshStats();
    }

    public void AddBlock( Block par_block, Building prefab_to_use )
    {
        int[] orientation_table = { 0, 90, 180, 270 };
        Building block_game_object = GameObject.Instantiate( prefab_to_use );

        if( !oldDistrictPolicyTable.ContainsKey( block_game_object.buildingType ) )
        {
            oldDistrictPolicyTable.Add( block_game_object.buildingType, 0 );
        }
        oldDistrictPolicyTable[ block_game_object.buildingType ] += 1;
        //if(!oldDistrictPolicyTable.ContainsKey( block_game_object.buildingType )

        block_game_object.transform.SetParent( transform, false );

        block_game_object.transform.localRotation = Quaternion.Euler( 0, orientation_table[ Random.Range( 0, orientation_table.Length ) ], 0 );

        AnimateNewBlockDrop( block_game_object.gameObject);

        blockTable.Add( par_block );
        par_block.transform.SetParent( transform, true );
        par_block.Initialize( block_game_object );
        par_block.SetParentDistrict( this );
        numberOfBlocks++;
        //par_block.GetComponent<MeshRenderer>().material.color = districtColor;
        UpdateDistrictBorder();
    }

    private void AnimateNewBlockDrop( GameObject par_block_game_object )
    {
        Vector3 intial_position = par_block_game_object.transform.localPosition;

        par_block_game_object.transform.Translate( Vector3.up * 1.0f, Space.Self );

        DOTween.Sequence()
            .Append( par_block_game_object.transform.DOScale( Vector3.zero, 0.2f ).From())
            .Append( par_block_game_object.transform.DOLocalMoveY( intial_position.y, 0.7f ).SetEase( Ease.OutCubic ) );
    }

    private void UpdateDistrictBorder()
    {
        List<GameObject> border_table = new List<GameObject>( borderTable );

        borderTable.Clear();

        foreach( Block block in blockTable )
        {
            foreach( KeyValuePair<Block.CardinalType, Block> neighbor_block in block.GetNeighborTable() )
            {
                if( ( neighbor_block.Value.GetParentDistrict() != null ) && ( neighbor_block.Value.GetParentDistrict() != this ) )
                {
                    GameObject current_border;

                    if( border_table.Count <= 0 )
                    {
                        border_table.Add( GameObject.Instantiate( GameManager.gameManager.resourceManager.borderPrefab ) );
                    }

                    current_border = border_table[ 0 ];
                    border_table.Remove( current_border );

                    current_border.transform.position = block.transform.position;

                    switch( neighbor_block.Key )
                    {
                        case Block.CardinalType.NORTH:
                            {
                                current_border.transform.localRotation = Quaternion.Euler( 0, 180, 0 );
                                break;
                            }
                        case Block.CardinalType.WEST:
                            {
                                current_border.transform.localRotation = Quaternion.Euler( 0, 270, 0 );
                                break;
                            }
                        case Block.CardinalType.SOUTH:
                            {
                                current_border.transform.localRotation = Quaternion.Euler( 0, 0, 0 );
                                break;
                            }
                        case Block.CardinalType.EAST:
                            {
                                current_border.transform.localRotation = Quaternion.Euler( 0, 90, 0 );
                                break;
                            }
                    }

                    current_border.transform.SetParent( block.transform, true );
                    current_border.SetActive( true );
                    //current_border.GetComponent<MeshRenderer>().material.color = districtColor;
                    borderTable.Add( current_border );
                }
            }
        }

        foreach( GameObject border in border_table )
        {
            border.SetActive( false );
        }
    }

    public List<Building.BuildingType> GetAvailablePolicy()
    {
        throw new NotImplementedException();
    }

    public void IncreasePolicy( PolicyType policy )
    {
        districtPolicyTable[policy] += 1;
        GameManager.gameManager.guiManager.statPanelLayout.DisplayDistrictStat( this );
    }

    public void DisplayNextChoice( PolicyType par_first_choice, PolicyType par_second_choice )
    {
        firstPolicyChoice = par_first_choice;
        secondPolicyChoice = par_second_choice;

        uiButton.gameObject.SetActive( true );
    }

    public void OnButtonClick()
    {
        QuestionPanelLayout question_panel = GameManager.gameManager.guiManager.questionPanelLayout;

        if( !question_panel.isActiveAndEnabled )
        {
            uiButton.gameObject.SetActive( false );
            question_panel.gameObject.SetActive( true );

            GameManager.gameManager.guiManager.questionPanelLayout.SetQuestion( string.Format( "Which way do you want to go ?" ), this, firstPolicyChoice, secondPolicyChoice );
        }
    }
}
