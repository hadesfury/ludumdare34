﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class CapabilityTypeQuantityDictionaryEntry : DictionaryEntry<CapabilityType, int>
{
    public CapabilityTypeQuantityDictionaryEntry(CapabilityType key, int value) : base(key, value)
    {

    }

    public static Dictionary<CapabilityType, int> toDictionary(List<CapabilityTypeQuantityDictionaryEntry> originalList)
    {
        Dictionary<CapabilityType, int> convertedDictionary = new Dictionary<CapabilityType, int>();

        foreach (var original_value in originalList)
        {
            convertedDictionary.Add(original_value.key, original_value.value);
        }

        return convertedDictionary;
    }
}