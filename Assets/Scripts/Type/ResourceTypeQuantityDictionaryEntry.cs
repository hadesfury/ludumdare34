﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class ResourceTypeQuantityDictionaryEntry : DictionaryEntry<ResourceType, int>
{
    public ResourceTypeQuantityDictionaryEntry(ResourceType key, int value) : base(key, value)
    {

    }

    public static Dictionary<ResourceType, int> toDictionary(List<ResourceTypeQuantityDictionaryEntry> originalList)
    {
        Dictionary<ResourceType, int> convertedDictionary = new Dictionary<ResourceType, int>();

        foreach (var original_value in originalList)
        {
            convertedDictionary.Add(original_value.key, original_value.value);
        }

        return convertedDictionary;
    }
}