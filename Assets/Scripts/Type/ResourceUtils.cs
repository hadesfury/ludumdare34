﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceUtils
{
    public static Dictionary<ResourceType, int> MergeResources(Dictionary<ResourceType, int> left, Dictionary<ResourceType, int> right)
    {
        Dictionary<ResourceType, int> merged_resources = new Dictionary<ResourceType, int>();

        foreach (KeyValuePair<ResourceType, int> left_resource in left)
        {
            int left_resource_quantity = left_resource.Value;
            int right_resource_quantity;
            if (!right.TryGetValue(left_resource.Key, out right_resource_quantity))
            {
                right_resource_quantity = 0;
            }
            merged_resources.Add(left_resource.Key, (left_resource_quantity + right_resource_quantity));
        }
        foreach (KeyValuePair<ResourceType, int> right_resource in right)
        {
            int merged_quantity;
            if (!merged_resources.TryGetValue(right_resource.Key, out merged_quantity))
            {
                merged_resources.Add(right_resource.Key, right_resource.Value);
            }
        }

        return merged_resources;
    }
}
