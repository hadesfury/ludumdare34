﻿using UnityEngine;
using System.Collections;

public enum ResourceType 
{
    //WOOD,       // Extacted from woods
    //STEEL,      // Extracted from mines
    MONEY,      // Produced by converters
    INHABITANT,  // People makes people
    POLLUTION,
    NONE
}
