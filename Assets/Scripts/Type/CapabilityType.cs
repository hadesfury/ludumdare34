﻿public enum CapabilityType
{
    EDUCATION,
    GROWTH,
    HEALTH,
    MONEY
}
