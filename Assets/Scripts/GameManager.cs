﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    public ResourceManager resourceManager;
    public GuiManager guiManager;

    public float tickDuration = 2.0f;
    public float minBetweenDecisions = 2.0f;
    public float timeBetweenNewDistrict = 10.0f;
    public bool isInAutoMode = false;
    public int maxDistrictValue = 1;

    [HideInInspector] public List<District> districtTable = new List<District>();
    [HideInInspector] public float lastDecisionTime = -1;

    private readonly Dictionary<int, Dictionary<int, Block>> blockTable = new Dictionary<int, Dictionary<int, Block>>();
    private float lastUpdateTime = -1;

    public readonly Dictionary<CapabilityType, int> capabilitiesScoreTable = new Dictionary<CapabilityType, int>();
    
    private string serverAddress = "http://ld34-ludumdare32.rhcloud.com/";
    private string playerName;
    private string playerId;
    private string lastWwwMessage;

    private const string PLAYER_NAME_ID = "PLAYER_NAME";

    void Awake()
    {
        if( !PlayerPrefs.HasKey( PLAYER_NAME_ID ) )
        {
            playerName = SystemInfo.deviceName;
        }
        else
        {
            playerName = PlayerPrefs.GetString( PLAYER_NAME_ID );
        }

        gameManager = this;
        guiManager.startMenu.gameObject.SetActive( true );
        guiManager.nameInputField.text = playerName;
    }

    void Start()
    {
        Block initial_block = GetBlock( 0, 0 );
        District initial_district = GameObject.Instantiate( resourceManager.districtPrefab );

        initial_district.Initialize( initial_block );

        districtTable.Add( initial_district );
        initial_district.districtId = 0;
        initial_district.name = string.Format( "District {0}", initial_district.districtId );
        gameObject.SetActive( false );
    }

    void OnEnable()
    {
        lastUpdateTime = Time.realtimeSinceStartup;
    }

    public void OnStartGameButton()
    {
        guiManager.startMenu.gameObject.SetActive( false );
        guiManager.scorePanel.gameObject.SetActive( true );
        guiManager.leaderboardLayout.gameObject.SetActive( false ); 
        guiManager.statPanelLayout.gameObject.SetActive( true );
        guiManager.statPanelLayout.DisplayDistrictStat( districtTable[0] );

        foreach( CapabilityType capability in Enum.GetValues( typeof( CapabilityType ) ) )
        {
            capabilitiesScoreTable.Add( capability, 0 );
        }

        guiManager.DisplayScore( capabilitiesScoreTable );
        gameObject.SetActive( true );
        lastUpdateTime = Time.realtimeSinceStartup;
        lastDecisionTime = Time.realtimeSinceStartup;

        playerName = guiManager.nameInputField.text;
        PlayerPrefs.SetString( PLAYER_NAME_ID, playerName );
        StartCoroutine( SendInitConnection() );
    }

    void Update()
    {
        float time_since_last_update = Time.realtimeSinceStartup - lastUpdateTime;

        while( time_since_last_update > tickDuration )
        {
            List<District> ticked_district_table = new List<District>();

            capabilitiesScoreTable.Clear();

            foreach( CapabilityType capability in Enum.GetValues( typeof( CapabilityType ) ) )
            {
                capabilitiesScoreTable.Add( capability, 0 );
            }

            bool has_created_new_block = false;

            while( ticked_district_table.Count < districtTable.Count )
            {
                District current_district_to_tick = null;

                while( (current_district_to_tick == null) || ticked_district_table.Contains( current_district_to_tick ))
                {
                    current_district_to_tick = districtTable[ Random.Range( 0, districtTable.Count ) ];
                }

                foreach( KeyValuePair<CapabilityType, int> capability in current_district_to_tick.capabilitiesScoreTable )
                {
                    capabilitiesScoreTable[capability.Key] += capability.Value;
                }
                
                if( current_district_to_tick.isFull )
                {
                    current_district_to_tick.Tick( !has_created_new_block );
                }
                else
                {
                    current_district_to_tick.Tick( !has_created_new_block );
                    has_created_new_block = true;
                }

                ticked_district_table.Add(current_district_to_tick);

                // ask question
                if(!current_district_to_tick.isFull && (Time.realtimeSinceStartup - lastDecisionTime > minBetweenDecisions))
                {
                    Array policy_table = Enum.GetValues( typeof( District.PolicyType ) );
                    District.PolicyType first_choice = ( District.PolicyType ) policy_table.GetValue( Random.Range( 0, policy_table.Length ) );
                    District.PolicyType second_choice = ( District.PolicyType ) policy_table.GetValue( Random.Range( 0, policy_table.Length ) );

                    if( !isInAutoMode )
                    {
                        while( first_choice == second_choice )
                        {
                            second_choice = ( District.PolicyType ) policy_table.GetValue( Random.Range( 0, policy_table.Length ) );
                        }

                        current_district_to_tick.DisplayNextChoice( first_choice, second_choice );
                    }
                    else
                    {
                        current_district_to_tick.IncreasePolicy(first_choice);
                    }

                    lastDecisionTime = Time.realtimeSinceStartup;
                }
            }

            guiManager.DisplayScore( capabilitiesScoreTable );
            
            if( districtTable.Count < maxDistrictValue) 
            {
                if( Time.realtimeSinceStartup / districtTable.Count > timeBetweenNewDistrict )
                {
                    AddDistrict();
                }
            }
            else
            {
                bool is_game_over = true;
                foreach( District district in districtTable )
                {
                    if( !district.isFull )
                    {
                        is_game_over = false;
                        break;
                    }
                }

                if( is_game_over )
                {
                    int ecology_score = capabilitiesScoreTable[ CapabilityType.EDUCATION ] + capabilitiesScoreTable[ CapabilityType.HEALTH ] - capabilitiesScoreTable[ CapabilityType.GROWTH ];
                    int wealth_score = capabilitiesScoreTable[ CapabilityType.EDUCATION ] + capabilitiesScoreTable[ CapabilityType.MONEY ];

                    StartCoroutine( SendScore( "ecology", ecology_score * 100 ) );
                    StartCoroutine( SendScore( "wealth", wealth_score * 100 ) );

                    gameObject.SetActive( false );
                    guiManager.statPanelLayout.gameObject.SetActive( false );
                    guiManager.gameOver.gameObject.SetActive( true );
                    guiManager.leaderboardLayout.gameObject.SetActive( true );
                }
            }

            time_since_last_update -= tickDuration;
            lastUpdateTime = Time.realtimeSinceStartup;
        }
    }

    public void OnResetButton()
    {
        SceneManager.LoadScene( 0 );
    }

    private void AddDistrict()
    {
        List<Block> free_block_table = new List<Block>();
        Color new_district_color = new Color( Random.Range( 0.0f, 1.0f ), Random.Range( 0.0f, 1.0f ), Random.Range( 0.0f, 1.0f ) );

        foreach( KeyValuePair<int, Dictionary<int, Block>> block_table in blockTable )
        {
            foreach( Block block in block_table.Value.Values )
            {
                if( block.GetParentDistrict() == null )
                {
                    free_block_table.Add( block );
                }
            }
        }

        if( free_block_table.Count > 0 )
        {
            District new_district = GameObject.Instantiate(resourceManager.districtPrefab);
            Block new_district_intial_block = free_block_table[ Random.Range( 0, free_block_table.Count ) ];
            
            new_district.districtId = districtTable.Count;
            new_district.name = string.Format( "District {0}", districtTable.Count );

            new_district.districtColor = new_district_color;
            districtTable.Add( new_district );
            new_district.transform.position = new_district_intial_block.transform.position;
            new_district.Initialize( new_district_intial_block );

            //new_district.AddBlock( new_district_intial_block );
        }
        //else
        //{
        //    Debug.LogError( string.Format( "World cannot expand" ) );
        //}
    }

    public Block GetBlock( int par_x_index, int par_y_index )
    {
        if( !blockTable.ContainsKey( par_x_index ) )
        {
            blockTable.Add( par_x_index, new Dictionary<int, Block>() );
        }

        if( !blockTable[ par_x_index ].ContainsKey( par_y_index ) )
        {
            GameObject block_game_object = new GameObject();//GameObject.CreatePrimitive( PrimitiveType.Cube ));
            Block block = block_game_object.AddComponent<Block>();

            block_game_object.AddComponent<BoxCollider>();

            block.xIndex = par_x_index;
            block.yIndex = par_y_index;
            block_game_object.name = string.Format( "Block [{0}, {1}]", par_x_index, par_y_index );
            block_game_object.transform.position = new Vector3( par_x_index, 0, par_y_index );
            blockTable[ par_x_index ].Add( par_y_index, block );
        }

        return blockTable[ par_x_index ][ par_y_index ];
    }

    IEnumerator SendInitConnection()
    {
        string
            json = string.Format( "{{\"playerName\": \"{0}\",\"gameRef\": \"{1}\"}}", playerName, "URBO" );
            //json = "{\"playerName\": \"" + playerName + "\" + \"gameRef\": \"URBO\" }";
        Dictionary<string, string>
            header_table = new Dictionary<string, string>();

        header_table.Add( "Content-Type", "application/json" );

        byte[]
            json_data = Encoding.ASCII.GetBytes( json.ToCharArray() );
        WWW
            www = new WWW( @"http://ld34-ludumdare32.rhcloud.com/game", json_data, header_table );

        yield return www;

        int index_start = www.text.IndexOf( "\"_id\":\"" );
        index_start += "\"_id\":\"".Length;
        int index_end = www.text.IndexOf( "\"", index_start + 1 );

        playerId = www.text.Substring( index_start, index_end - index_start );

        print( www.text );
        lastWwwMessage = www.text;
        print( playerId );
        
        StartCoroutine( SendIsConnectionAlive() );
    }

    IEnumerator SendIsConnectionAlive()
    {
        while( true )
        {
            if( playerId != null )
            {
                string
                    json = string.Format( "{{\"playerName\": \"{0}\",\"gameRef\": \"{1}\"}}", playerName, "URBO" );
                    //json = "{\"playerName\": \"" + playerName + "\" + \"gameRef\": \"URBO\" }";
                Dictionary<string, string>
                    header_table = new Dictionary<string, string>();

                header_table.Add( "Content-Type", "application/json" );

                byte[]
                    json_data = Encoding.ASCII.GetBytes( json.ToCharArray() );
                WWW
                    www = new WWW( @"http://ld34-ludumdare32.rhcloud.com/game/" + playerId, json_data, header_table );

                yield return www;

                print( www.text );
                lastWwwMessage = www.text;
            }

            yield return new WaitForSeconds( 10 );
        }
    }

    IEnumerator SendScore(string score_name, int score)
    {
        string
           json = string.Format("{{\"playerName\": \"{0}\",\"gameRef\": \"{1}\" ,\"score\": \"{2}\"}}",playerName,"URBO_"+score_name,score);
        Dictionary<string, string>
            header_table = new Dictionary<string, string>();

        header_table.Add( "Content-Type", "application/json" );

        byte[]
            json_data = Encoding.ASCII.GetBytes( json.ToCharArray() );
        WWW
            www = new WWW( @"http://ld34-ludumdare32.rhcloud.com/score", json_data, header_table );

        yield return www;

        print( www.text );
        lastWwwMessage = www.text;
    }
}
