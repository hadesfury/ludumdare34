﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class ConverterConfiguration : MonoBehaviour
{
    // External configuration
    [HideInInspector] public List<ResourceTypeQuantityDictionaryEntry> _sourceTypes;
    public List<ResourceTypeQuantityDictionaryEntry> _targetTypes;
    public int conversionRate = 0;

    // Internal configuration
    private Dictionary<ResourceType, int> internalSourceTypes;
    public Dictionary<ResourceType, int> sourceTypes
    {
        get
        {
            if(internalSourceTypes == null)
            {
                Assert.IsNotNull(_sourceTypes);
                internalSourceTypes = ResourceTypeQuantityDictionaryEntry.toDictionary(_sourceTypes);
            }
            return internalSourceTypes;
        }
    }

    private Dictionary<ResourceType, int> internalTargetTypes;
    public Dictionary<ResourceType, int> targetTypes
    {
        get
        {
            if(internalTargetTypes == null)
            {
                Assert.IsNotNull(_targetTypes);
                internalTargetTypes = ResourceTypeQuantityDictionaryEntry.toDictionary(_targetTypes);
            }
            return internalTargetTypes;
        }
    }

    public void Convert( int elasped_time, out Dictionary<ResourceType, int> consumed_resources , out Dictionary<ResourceType, int> produced_resources)
    {
        consumed_resources = new Dictionary< ResourceType, int >();
        foreach ( KeyValuePair< ResourceType, int > source_type in sourceTypes )
        {
            consumed_resources.Add( source_type.Key, source_type.Value * ( conversionRate / elasped_time ) );
        }

        produced_resources = new Dictionary< ResourceType, int >();
        foreach (KeyValuePair<ResourceType, int> target_type in targetTypes)
        {
            produced_resources.Add(target_type.Key, target_type.Value * (conversionRate / elasped_time) );
        }
    }

    public bool CanConvert(int elasped_time, Dictionary< ResourceType, int > available_resources )
    {
        foreach( KeyValuePair< ResourceType, int > required_resource in sourceTypes)
        {
            int available_resource;
            bool is_resource_present = available_resources.TryGetValue( required_resource.Key, out available_resource );
            if ( (is_resource_present) && (available_resource < required_resource.Value * ( conversionRate / elasped_time ) ) )
            {
                return false;
            }
        }

        return true;
    }
}
