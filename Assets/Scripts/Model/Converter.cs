﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class Converter : MonoBehaviour
{
    // External Configuration
    [HideInInspector] public ConverterConfiguration configuration;

    // External State
    public float producingRatio = 1.0f; // TODO implement a variable ratio

    void Start()
    {
        // Validate the provided configuration
        Assert.IsNotNull( configuration );

    }

    public Dictionary<ResourceType, int> Convert(int elapsed_time, ref Dictionary<ResourceType, int>  available_resources)
    {
        // Just to be sure !
        if (configuration == null)
        {
            Debug.LogError( "Hey Converter has no configuration - WTF ?!?" );
            return new Dictionary< ResourceType, int >();
        }

        Dictionary<ResourceType, int> consumed_resources;
        Dictionary<ResourceType, int> produced_resources;

        Assert.IsTrue( configuration._sourceTypes.Count == 0, "Proper resources consumsion not implemented" );

        configuration.Convert(elapsed_time, out consumed_resources, out produced_resources);

        return produced_resources;
    }
}
