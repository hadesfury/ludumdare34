﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class TankerConfiguration : MonoBehaviour
{
    // External configuration
    public List<ResourceTypeQuantityDictionaryEntry> _resourceCapacities;

    // Internal Configuration
    private Dictionary<ResourceType, int> internalResourceCapacities;
    public Dictionary<ResourceType, int> resourceCapacities
    {
        get
        {
            if(internalResourceCapacities == null)
            {
                Assert.IsNotNull(_resourceCapacities);
                internalResourceCapacities = ResourceTypeQuantityDictionaryEntry.toDictionary(_resourceCapacities);
            }
            return internalResourceCapacities;
        }
    }

    public int GetCapacity( ResourceType resource_type )
    {
        int max_capacity;
        bool is_able = resourceCapacities.TryGetValue( resource_type, out max_capacity);
        if( !is_able )
        {
            return 0;
        }
        return max_capacity;
    }
}
