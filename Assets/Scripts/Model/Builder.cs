﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class Builder : MonoBehaviour
{

    // External Configuration
    [HideInInspector] public BuilderConfiguration configuration;

    // External State
    public bool isCompleted = false;
    public float completionRatio = 0.0f;
    public int buildingTime = 0;

    // Internal Configuration
    private Dictionary<ResourceType, int> requiredResourcesToBuild;

    // Internal State
    private int requiredCompletionPoints = 0;
    private int actualCompletionPoints = 0;

    void Start()
    {
        // Validate the provided configuration
        Assert.IsNotNull( configuration );

        // Initialize requiredResourcesToBuild based on its build costs
        this.requiredResourcesToBuild = new Dictionary<ResourceType, int>();
        foreach (KeyValuePair<ResourceType, int> build_cost in configuration.buildCosts)
        {
            this.requiredResourcesToBuild.Add(build_cost.Key, build_cost.Value);
            requiredCompletionPoints = requiredCompletionPoints + build_cost.Value;
        }
    }

    public Dictionary<ResourceType, int> Build(int elapsed_time, ref Dictionary<ResourceType, int> available_resources)
    {
        // No-op - if it's already completed !
        if (isCompleted)
        {
            return new Dictionary<ResourceType, int>();
        }

        // Just to be sure !
        if (configuration == null)
        {
            Debug.LogError("Hey Builder has no configuration - WTF ?!?");
            return new Dictionary<ResourceType, int>();
        }

        // Compute *potentially* consumed resources during the elapsed time
        Dictionary<ResourceType, int> to_consume_resources;
        Dictionary<ResourceType, int> consumed_resources = new Dictionary< ResourceType, int >(); 
        configuration.Build(elapsed_time, out to_consume_resources);

        if( to_consume_resources.Count == 0 )
        {
            completionRatio = 1.0f;
        }

        foreach (KeyValuePair<ResourceType, int> to_consume_resource in to_consume_resources)
        {
            // Check what remains to build
            int required_resource = 0;
            if (!requiredResourcesToBuild.TryGetValue(to_consume_resource.Key, out required_resource)) // Hey WTF ??
            {
                Debug.LogWarning(
                    string.Format(
                        "Hey you are consuming resources {0} that are not declared as required, WTF ?!?", to_consume_resource.Key));
            }
            else // Convert into *effectively* consumed resource
            {
                int available_resource;
                bool is_available = available_resources.TryGetValue(to_consume_resource.Key, out available_resource);
                if (is_available)
                {
                    int consumed_resource = Mathf.Min(required_resource, Mathf.Min(to_consume_resource.Value, available_resource));
                    if (consumed_resource > 0)
                    {
                        // Ok we can decrement now !
                        requiredResourcesToBuild[to_consume_resource.Key] -= consumed_resource;
                        available_resources[to_consume_resource.Key] -= consumed_resource;

                        // Refresh completion ratio 
                        if( requiredResourcesToBuild[ to_consume_resource.Key ] == 0 )
                        {
                            completionRatio = 1.0f;
                        }
                        else
                        {
                            actualCompletionPoints += consumed_resource;
                            completionRatio = actualCompletionPoints / (float)requiredCompletionPoints;
                        }
                        consumed_resources.Add(to_consume_resource.Key, consumed_resource);
                    }
                }
            }
        }

        // Refresh state
        buildingTime += elapsed_time;
        isCompleted = ( ( buildingTime >= configuration.buildTime ) && ( completionRatio > 0.99f ) );

        return consumed_resources;
    }
}
