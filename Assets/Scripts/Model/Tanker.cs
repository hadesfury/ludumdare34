﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine.Assertions;

public class Tanker : MonoBehaviour
{
    // External configuration
    [HideInInspector] public TankerConfiguration configuration;

    // External state
    public float fillRatio = 0.0f;

    private Dictionary< ResourceType, int > containedResources;

    // Use this for initialization
    void Start()
    {
        // Validate provided configuration
        Assert.IsNotNull( configuration );
        containedResources = new Dictionary< ResourceType, int >();
        foreach ( ResourceTypeQuantityDictionaryEntry resource_capacity in configuration._resourceCapacities )
        {
            containedResources.Add( resource_capacity.key, 0 );
        }
    }

    public int Push( ResourceType resource_type, int requested_quantity )
    {
        int current_quantity;
        if (!containedResources.TryGetValue(resource_type, out current_quantity))
        {
            return 0;
        }
        int max_capacity = configuration.GetCapacity( resource_type );
        int remaining_capacity = max_capacity - current_quantity;
        if(remaining_capacity < 0 )
        {
            Debug.LogError(
                string.Format("Hey you're getting a negative remaining capacity for resource {0} - WTF ?!?", resource_type));
            return 0;
        }
        // What if the requested quantity is over the remaining capacity ?
        int storable_quantity = requested_quantity;
        if( remaining_capacity <= requested_quantity )
        {
            storable_quantity = remaining_capacity;
        }
        containedResources[resource_type] = current_quantity + storable_quantity;

        return storable_quantity;
    }


    public int Pop( ResourceType resource_type, int requested_quantity)
    {
        int contained_quantity;
        bool is_contained = containedResources.TryGetValue( resource_type, out contained_quantity );
        if( !is_contained )
        {
            return 0;
        }

        int provided_quantity = Mathf.Min( requested_quantity, contained_quantity );
        
        containedResources[ resource_type ] = contained_quantity - provided_quantity;

        return provided_quantity;
    }
}
