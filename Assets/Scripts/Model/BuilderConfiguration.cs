﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine.Assertions;

public class BuilderConfiguration : MonoBehaviour
{
    // External configuration
    public List<CapabilityTypeQuantityDictionaryEntry> _minimumCapabilities;
    public List<ResourceTypeQuantityDictionaryEntry> _buildCosts;
    public List<CapabilityTypeQuantityDictionaryEntry> _producedCapabilities;
    public int buildTime;

    // Internal configuration
    //private Dictionary<CapabilityType, int> internalProducedCapabilities;
    private Dictionary< CapabilityType, int > internalMinimumCapabilities;
    public Dictionary< CapabilityType, int> minimumCapabilities
    {
        get
        {
            if (internalMinimumCapabilities == null)
            {
                Assert.IsNotNull(_minimumCapabilities);
                internalMinimumCapabilities = CapabilityTypeQuantityDictionaryEntry.toDictionary(_minimumCapabilities);
            }
            return internalMinimumCapabilities;
        }
    }

    private Dictionary<ResourceType, int> internalBuildCosts;
    public Dictionary< ResourceType, int > buildCosts
    {
        get
        {
            if(internalBuildCosts == null)
            {
                Assert.IsNotNull(_buildCosts);
                internalBuildCosts = ResourceTypeQuantityDictionaryEntry.toDictionary(_buildCosts);
            }
            return internalBuildCosts;
        }
    }

    //void Awake()
    //{
    //    if( _producedCapabilities != null )
    //    {
    //        internalProducedCapabilities = CapabilityTypeQuantityDictionaryEntry.toDictionary( _producedCapabilities );
    //    }
    //}

    public void Build(int elasped_time, out Dictionary<ResourceType, int> consumed_resources)
    {
        consumed_resources = new Dictionary< ResourceType, int >();

        foreach (KeyValuePair<ResourceType, int> build_cost in buildCosts)
        {
            int cost = build_cost.Value * ( buildTime / elasped_time );

            if( cost > 0 )
            {
                consumed_resources.Add( build_cost.Key, cost );
            }
        }
    }

    public bool CanBuild( Dictionary< ResourceType, int > available_resources, Dictionary< CapabilityType, int > available_capabilities )
    {
        foreach (KeyValuePair<ResourceType, int> required_resource in buildCosts)
        {
            int available_resource;
            bool is_resource_present = available_resources.TryGetValue(required_resource.Key, out available_resource);
            if (!is_resource_present || (available_resource < required_resource.Value))
            {
                return false;
            }
        }

        foreach (KeyValuePair<CapabilityType, int> required_capability in minimumCapabilities)
        {
            int capability_level;
            bool is_capability_present = available_capabilities.TryGetValue(required_capability.Key, out capability_level);
            if (!is_capability_present || (capability_level < required_capability.Value))
            {
                return false;
            }
        }

        return true;
    }
}
