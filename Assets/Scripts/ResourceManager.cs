﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour
{
    public GameObject borderPrefab;
    public District districtPrefab;
    public List<Building> buildingTable;
    
    public readonly Dictionary<Building.BuildingType, List<Building>> sortedBuildingTable = new Dictionary<Building.BuildingType, List<Building>>();

    void Awake()
    {
        foreach( Building.BuildingType building_type in Enum.GetValues( typeof( Building.BuildingType ) ) )
        {
            sortedBuildingTable.Add( building_type, new List<Building>() );
        }

        foreach( Building building in buildingTable )
        {
            sortedBuildingTable[ building.buildingType ].Add( building );
        }

        foreach( List<Building> building in sortedBuildingTable.Values )
        {
            building.Sort( CompareBuildings );
        }
        //print( "test" );
    }

    private int CompareBuildings( Building par_x, Building par_y )
    {
        return par_x.buildingLevel - par_y.buildingLevel;
    }
}
