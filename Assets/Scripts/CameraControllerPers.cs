﻿using UnityEngine;
using System.Collections;

public class CameraControllerPers : MonoBehaviour
{
	public Transform cameraTarget;
	public Vector2 panSpeed;

	[Header("Zoom")]
	public float minZoom = 1;
	public float maxZoom = 10;
	public float zoomStep = 1;
	public float ctrlZoomSpeed = 10.0f;
    public float pinchZoomSpeed = 20.0f;

	private Plane groundPlane;

	private Vector3
		previousMousePosition,
		newMousePosition,
		previousTargetPosition,
		previousCameraPosition;
	private new Camera camera;

	private float current_zoom_level;

    private bool panning = false;

	void Awake()
	{
		camera = GetComponent<Camera>();
		previousTargetPosition = cameraTarget.position;
		previousCameraPosition = transform.position;
		groundPlane.SetNormalAndPosition(Vector3.up, Vector3.zero);
		current_zoom_level = 0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Pan Keyboard
		{
			float horizontal_axis = Input.GetAxis( "Horizontal" );
			float vertical_axis = Input.GetAxis( "Vertical" );
			Vector3 forwardVector = transform.forward;
			forwardVector.y = 0f;
			cameraTarget.Translate( horizontal_axis * transform.right * panSpeed.x * Time.deltaTime );
			cameraTarget.Translate( vertical_axis * forwardVector * panSpeed.y * Time.deltaTime );
		}

		//Pan tactile
        if (Input.GetMouseButtonDown (0) && (Input.touchCount != 2))
		{
            panning = true;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float rayDistance;
			if (groundPlane.Raycast(ray, out rayDistance))
			{
				previousMousePosition = ray.GetPoint(rayDistance);
			}
		}
        if (Input.GetMouseButton(0) && (Input.touchCount != 2) )
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float rayDistance;
			if (groundPlane.Raycast(ray, out rayDistance))
			{
				newMousePosition = ray.GetPoint(rayDistance);
			}

			Vector3 delta_mouse = newMousePosition - previousMousePosition;
			if (Input.GetKey(KeyCode.LeftControl)	|| Input.GetKey(KeyCode.RightControl))
			{
				current_zoom_level -= Vector3.Dot(delta_mouse, transform.forward) * ctrlZoomSpeed;

			}
			else
			{
				cameraTarget.position = previousTargetPosition - delta_mouse;
			}
		}
		previousTargetPosition = cameraTarget.position;

        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            current_zoom_level -= deltaMagnitudeDiff * pinchZoomSpeed;
        }

		//Zoom
		{
			float mouse_scroll_wheel = Input.GetAxis( "Mouse ScrollWheel" );

			if( mouse_scroll_wheel > 0  || Input.GetKey(KeyCode.KeypadPlus))
			{
				current_zoom_level += zoomStep;
			}
			else if( mouse_scroll_wheel < 0 || Input.GetKey(KeyCode.KeypadMinus))
			{
				current_zoom_level -= zoomStep;
			}
			transform.localPosition = new Vector3(0f, 0f, Mathf.Clamp( current_zoom_level, minZoom, maxZoom ));
			current_zoom_level = transform.localPosition.z;
		}
	}
}
