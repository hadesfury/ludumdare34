﻿using System;
using System.Diagnostics;

using UnityEditor;

using UnityEngine;

using Debug = UnityEngine.Debug;

public class BuildScript
{
    private static readonly String WINDOWS_BUILD_PATH = "Versions/Windows/urbo.exe";
    private static readonly String OSX_BUILD_PATH = "Versions/OSX/urbo.app";
    private static readonly String LINUX_BUILD_PATH = "Versions/Linux/urbo";
    private static readonly String WEBGL_BUILD_PATH = "Versions/WebGL/";
    
    private static readonly String[] levels = null;

    private static bool hasResourcesBeenMade = false;


    [MenuItem("Build/BuildAllVersion")]
    public static void Buildgame()
    {
        BuildWindows();
        BuildOsx();
        BuildLinux();
    }

    [MenuItem("Build/BuildWindows")]
    public static void BuildWindows()
    {
        BuildPipeline.BuildPlayer(levels, WINDOWS_BUILD_PATH, BuildTarget.StandaloneWindows, BuildOptions.None);
    }

    [MenuItem("Build/BuildOSX")]
    public static void BuildOsx()
    {
        BuildPipeline.BuildPlayer( levels, OSX_BUILD_PATH, BuildTarget.StandaloneOSXUniversal, BuildOptions.None );
    }

    [MenuItem("Build/BuildLinux")]
    public static void BuildLinux()
    {
        BuildPipeline.BuildPlayer( levels, LINUX_BUILD_PATH, BuildTarget.StandaloneLinuxUniversal, BuildOptions.None );
    }

    [MenuItem("Build/BuildWebGL")]
    public static void BuildWebGL()
    {
        BuildPipeline.BuildPlayer( levels, WEBGL_BUILD_PATH, BuildTarget.WebGL, BuildOptions.None );
    }
}
