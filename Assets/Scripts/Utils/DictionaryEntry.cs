using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class DictionaryEntry<K, V>
{
    public K key;
    public V value;

    public DictionaryEntry(K key, V value)
    {
        this.key = key;
        this.value = value;
    }

    public override string ToString()
    {
        return string.Format("First: {0}, Second: {1}", key, value);
    }
}

