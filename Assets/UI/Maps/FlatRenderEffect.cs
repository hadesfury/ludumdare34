using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FlatRenderEffect : MonoBehaviour
{
	public Color color;
	private Material material;
	
	// Creates a private material used to the effect
	void Awake ()
	{
		material = new Material( Shader.Find("Hidden/flatRender") );
	}
	
	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
		material.SetColor("_Color", color);
		Graphics.Blit (source, destination, material);
	}
}